<?php

session_start();

$string = '';

for ($i = 0; $i < 5; $i++) {
	// this numbers refer to numbers of the ascii table (lower case)
	$string .= chr(rand(97, 122));
}

$_SESSION['rand_code'] = $string;

$dir = 'fonts/';

$image = imagecreatetruecolor(170, 60);
$black = imagecolorallocate($image, 0, 0, 0);
$color = imagecolorallocate($image, 255, 85, 0); // brave browser orange
$white = imagecolorallocate($image, 0, 0, 0);

imagefilledrectangle($image,0,0,200,100,$white);
imagettftext ($image, 30, 0, 10, 40, $color, $dir."ABOVE  - PERSONAL USE ONLY.ttf", $_SESSION['rand_code']);

header("Content-type: image/png");
imagepng($image);

if(isset($_POST['submit'])) {
     
    if(!empty($_POST['name']) && !empty($_POST['email']) && !empty($_POST['message']) && !empty($_POST['code'])) {
     
        if($_POST['code'] == $_SESSION['rand_code']) {
         
            // send email
            $accept = "Thank you for contacting me.";
         
        } else {
         
            $error = "Please verify that you typed in the correct code.";
         
        }
         
    } else {
     
        $error = "Please fill out the entire form.";
     
    }
 
}
?>